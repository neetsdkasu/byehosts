fsc ^
    --codepage:65001 ^
    --target:winexe ^
    --out:byehosts.exe ^
    --warnaserror+ ^
    --debug- ^
    --optimize+ ^
    --crossoptimize+ ^
    --standalone ^
    AssemblyInfo.fs ^
    Main.fs
