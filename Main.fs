//////////////////////////////////////////////
// title  :  byehosts
// author : Leonardone @ NEETSDKASU
//////////////////////////////////////////////

open System.ComponentModel
open System.Drawing
open System.IO
open System.Windows.Forms

let appTitle() : string = Application.ProductName

type Result<'a, 'b> =
    | Ok  of 'a
    | Err of 'b

type ResultBuilder() =
    member self.Bind(expr, func) =
        match expr with
        | Ok  res -> func res
        | Err err -> Err err
    member self.ReturnFrom expr = expr
let result = new ResultBuilder()

module String =
    let rev (s: string) : string =
        let ca = s.ToCharArray() |> Array.rev
        new string(ca)

module Param =
    let HostsFilePath : string = @"C:\Windows\System32\drivers\etc\hosts"

    type Param(input: string, output: string, backup: string) =
        member val Input  : string = input
        member val Output : string = output
        member val Backup : string = backup

    let checkExists (path: string) : Result<unit, exn> =
        if File.Exists path then
            Ok ()
        else
            Err (Failure <| sprintf "no such file: %+A" path)

    let getFilepath (path: string) : Result<string, exn> =
        try
            Ok (Path.GetFullPath path)
        with
            err -> Err err

    let (|InputFlag|_|) (flag: string) : unit option =
        match flag.ToLower() with
        | "-i" | "-in" | "-input" | "--input"
        | "/i" | "/in" | "/input" -> Some ()
        | _ -> None

    let (|OutputFlag|_|) (flag: string) : unit option =
        match flag.ToLower() with
        | "-o" | "-out" | "-output" | "--output"
        | "/o" | "/out" | "/output" -> Some ()
        | _ -> None

    let (|BackupFlag|_|) (flag: string) : unit option =
        match flag.ToLower() with
        | "-b" | "-backup" | "--backup"
        | "/b" | "/backup" -> Some ()
        | _  -> None

    let parse (args: string array) : Result<Param, exn> =
        let args = Array.toList args
        let mutable input  = HostsFilePath
        let mutable output = "new_hosts"
        let mutable backup = "backup_hosts"
        let rec parse args =
            match args with
            | InputFlag :: path :: rest ->
                input <- path
                parse rest
            | OutputFlag :: path :: rest ->
                output <- path
                parse rest
            | BackupFlag :: path :: rest ->
                backup <- path
                parse rest
            | f :: _ -> Err (Failure <| sprintf "unknown param: %s" f)
            | [] ->
                result {
                    let! input  = getFilepath input
                    let! output = getFilepath output
                    let! backup = getFilepath backup
                    do! checkExists input
                    return! Ok (new Param(input, output, backup))
                }
        parse args


module Ignored =
    type Ignored(blocked: bool, hostname: string, comment: string, created: bool) =
        member val Key     : string = String.rev hostname
        member val Blocked : bool   = blocked with get, set
        member val Host    : string = hostname
        member val Comment : string = comment with get, set
        member val Created : bool   = created
        member self.BlockModified   with get() : int = if blocked <> self.Blocked then 1 else 0
        member self.CommentModified with get() : int = if comment <> self.Comment then 2 else 0
        member self.Modified
            with get() : int =
                self.BlockModified ||| self.CommentModified ||| (if created then 4 else 0)
        override self.ToString() : string =
            let comment = if self.Comment = "" then "" else ("\t# " + self.Comment)
            if self.Blocked then
                sprintf "\t127.0.0.1\t%s%s" self.Host comment
            else
                sprintf "#\t127.0.0.1\t%s%s" self.Host comment

    let separator = [|'\t'|]

    let parse (line: string) : Ignored option =
        match line.Split(separator, System.StringSplitOptions.RemoveEmptyEntries) with
        | [|"127.0.0.1"; hostname|] ->
            Some (new Ignored(true, hostname, "", false))
        | [|"127.0.0.1"; hostname; comment|] when comment.StartsWith("#") ->
            Some (new Ignored(true, hostname, comment.Substring(1).Trim(), false))
        | [|"#"; "127.0.0.1"; hostname|] ->
            Some (new Ignored(false, hostname, "", false))
        | [|"#"; "127.0.0.1"; hostname; comment|] when comment.StartsWith("#") ->
            Some (new Ignored(false, hostname, comment.Substring(1).Trim(), false))
        | _ ->
            None


module Hosts =
    let beginSignature : string = "# BEGIN(BYEHOSTS)"
    let endSignature   : string = "# END(BYEHOSTS)"

    let update (m: Map<string, Ignored.Ignored>) (v: Ignored.Ignored) : Ignored.Ignored =
        match Map.tryFind v.Key m with
        | None   -> v
        | Some x ->
            x.Blocked <- v.Blocked
            x.Comment <- v.Comment
            x

    type Hosts(header: string array, footer: string array, ignores: Map<string, Ignored.Ignored>) =
        member val Header : string array = header
        member val Footer : string array = footer
        member val Ignores : Map<string, Ignored.Ignored> = ignores with get, set
        member self.Modified
            with get() : bool = self.Ignores |> Map.exists (fun _ ig -> ig.Modified <> 0)
        member self.Update(newValues: Ignored.Ignored array) : Ignored.Ignored array =
            let (ignores, newValues) =
                Array.fold (fun (s, m) v ->
                    let v = update s v
                    (Map.add v.Key v s, Map.add v.Key v m)
                ) (self.Ignores, Map.empty) newValues
            self.Ignores <- ignores
            Map.toArray newValues |> Array.map snd
        override self.ToString() : string =
            let writer = new StringWriter()
            self.Header |> Array.iter (fun line -> writer.WriteLine(line))
            writer.WriteLine(beginSignature)
            self.Ignores |> Map.iter (fun _ ig -> writer.WriteLine(ig))
            writer.WriteLine(endSignature)
            self.Footer |> Array.iter (fun line -> writer.WriteLine(line))
            writer.ToString()

    let parse (lines: string array) : Result<Hosts, exn> =
        let headerPos = Array.tryFindIndex ((=) beginSignature) lines
        let footerPos = Array.tryFindIndex ((=) endSignature) lines
        match (headerPos, footerPos) with
        | (None, None) -> Ok (new Hosts(lines, [||], Map.empty))
        | (None, _) | (_, None) -> Err (Failure "invalid hosts file")
        | (Some headerPos, Some footerPos) ->
            let header = lines.[..headerPos - 1]
            let footer = lines.[footerPos + 1..]
            let ignores =
                lines.[headerPos..footerPos]
                |> Seq.map Ignored.parse
                |> Seq.filter Option.isSome
                |> Seq.map (Option.get >> (fun ig -> (ig.Key, ig)))
                |> Map.ofSeq
            Ok (new Hosts(header, footer, ignores))


module UI =
    let setChildren (components: IContainer) (parent: Control) (children: Control array) : unit =
        for child in children do
            parent.Controls.Add(child)
            components.Add(child)


    type LTPanel(components: IContainer) as self =
        inherit Panel()
        let label = new Label(Left      = 0
                             ,Top       = 0
                             ,Width     = 50
                             ,TextAlign = ContentAlignment.MiddleLeft
                             )
        let textBox = new TextBox(Left     = label.Width
                                 ,Top      = 0
                                 ,Width    = self.ClientRectangle.Width - label.Width
                                 ,Anchor   = (AnchorStyles.Left ||| AnchorStyles.Right)
                                 ,ReadOnly = true
                                 )
        do
            self.Height <- max label.Height textBox.Height
            setChildren components self [|label; textBox|]

        member self.Label
            with get() : string = label.Text
            and set(value: string) : unit = label.Text <- value
        member self.TextBox
            with get() : string = textBox.Text
            and set(value: string) : unit = textBox.Text <- value
        member val ToolTipTarget : Control = textBox :> Control

    type CheckedFilter(components: IContainer) as self =
        inherit Panel()
        let label = new Label(Left      = 0
                             ,Top       = 0
                             ,Width     = 60
                             ,Text      = "blocked: "
                             ,TextAlign = ContentAlignment.MiddleLeft
                             )
        let radioButton1 = new RadioButton(Left    = label.Width
                                          ,Top     = 0
                                          ,Width   = 100
                                          ,Text    = "No use"
                                          ,Checked = true
                                          )
        let radioButton2 = new RadioButton(Left  = radioButton1.Left + radioButton1.Width
                                          ,Top   = 0
                                          ,Width = 100
                                          ,Text  = "checked"
                                          )
        let radioButton3 = new RadioButton(Left  = radioButton2.Left + radioButton2.Width
                                          ,Top   = 0
                                          ,Width = 100
                                          ,Text  = "unchecked"
                                          )
        do
            self.Height <- max label.Height radioButton1.Height
            setChildren components self [|label; radioButton1; radioButton2; radioButton3|]
        member self.CheckedValue
            with get() : bool option =
                if radioButton1.Checked then
                    None
                else
                    Some radioButton2.Checked


    type MatchedFilter(components: IContainer) as self =
        inherit Panel()
        let label = new Label(Left      = 0
                             ,Top       = 0
                             ,Width     = 60
                             ,Text      = "matched: "
                             ,TextAlign = ContentAlignment.MiddleLeft
                             )
        let radioButton1 = new RadioButton(Left    = label.Width
                                          ,Top     = 0
                                          ,Width   = 100
                                          ,Text    = "No use"
                                          ,Checked = true
                                          )
        let radioButton2 = new RadioButton(Left  = radioButton1.Left + radioButton1.Width
                                          ,Top   = 0
                                          ,Width = 100
                                          ,Text  = "Comment"
                                          )
        let radioButton3 = new RadioButton(Left  = radioButton2.Left + radioButton2.Width
                                          ,Top   = 0
                                          ,Width = 100
                                          ,Text  = "Host Name"
                                          )
        let radioButton4 = new RadioButton(Left  = radioButton3.Left + radioButton3.Width
                                          ,Top   = 0
                                          ,Width = 100
                                          ,Text  = "Any"
                                          )
        let textBox = new TextBox(Left   = label.Width
                                 ,Top    = max label.Height radioButton1.Height
                                 ,Width  = self.Width - label.Width
                                 ,Anchor = (AnchorStyles.Left ||| AnchorStyles.Top ||| AnchorStyles.Right)
                                 )
        do
            self.Height <- textBox.Top + textBox.Height
            setChildren components self
                [|label; radioButton1; radioButton2; radioButton3; radioButton4; textBox|]
        member self.TargetString  with get() : string = textBox.Text
        member self.MatchedValue
            with get() : bool * bool =
                let comment  = radioButton2.Checked || radioButton4.Checked
                let hostname = radioButton3.Checked || radioButton4.Checked
                (comment, hostname)
        member val ToolTipTarget : Control = textBox :> Control

    type FilterFunc = Ignored.Ignored -> bool

    type Filter(components: IContainer) as self =
        inherit GroupBox(Text = "Filter:")
        let padding = 20
        let mutable checkedValue = 1
        let mutable matchedValue = 1
        let checkedFilter = new CheckedFilter(components
                                             ,Left   = padding
                                             ,Top    = padding
                                             ,Width  = self.Width - padding * 2
                                             ,Anchor = (AnchorStyles.Left ||| AnchorStyles.Top ||| AnchorStyles.Right)
                                             )
        let matchedFilter = new MatchedFilter(components
                                             ,Left   = padding
                                             ,Top    = checkedFilter.Top + checkedFilter.Height
                                             ,Width  = self.Width - padding * 2
                                             ,Anchor = (AnchorStyles.Left ||| AnchorStyles.Top ||| AnchorStyles.Right)
                                             )
        let applyButton = new Button(Left   = self.ClientRectangle.Width - padding - 100
                                    ,Top    = matchedFilter.Top + matchedFilter.Height + 10
                                    ,Width  = 100
                                    ,Text   = "Apply"
                                    ,Anchor = (AnchorStyles.Top ||| AnchorStyles.Right)
                                    )
        do
            self.Height <- applyButton.Top + applyButton.Height + padding / 2
            setChildren components self [|checkedFilter; matchedFilter; applyButton|]
        let separator = [|' '|]
        let makeFilterFunc() : FilterFunc =
            let checkedValue  = checkedFilter.CheckedValue
            let matchedValue  = matchedFilter.MatchedValue
            let targetStrings = matchedFilter.TargetString.Split(separator, System.StringSplitOptions.RemoveEmptyEntries)
            let matches (value: string) : bool =
                Array.exists (fun target -> value.Contains(target)) targetStrings
            fun item ->
                match checkedValue with
                | Some blocked -> item.Blocked = blocked
                | None         -> true
                &&
                match matchedValue with
                | (true, _) when matches item.Comment -> true
                | (_, true) when matches item.Host -> true
                | (true, _) | (_, true) -> false
                | _ -> true
        member val ApplyButton : Button = applyButton
        member self.GetFilterFunc() : FilterFunc = makeFilterFunc()
        member val ToolTipTarget : Control = matchedFilter.ToolTipTarget


    type Notifier<'a> = ('a -> unit) option // Notifierじゃ名前と役割が一致してなさそう

    let notify<'a> (notifier: Notifier<'a>) (v: 'a) : unit = Option.iter (fun f -> f v) notifier

    let modStarUpd = Array.get [|""; "*-"; "-*"; "**"|]
    let modStarNew = Array.get [|"--"; "*-"; "-*"; "**"|]
    let modStars modified =
        if (modified &&& 4) = 0 then // マジックナンバー？よくない
            modStarUpd modified
        else
            modStarNew (modified &&& 3)

    type IgnoresView(notifier: Notifier<unit> ref) as self =
        inherit ListView(View          = View.Details
                        ,CheckBoxes    = true
                        ,MultiSelect   = false
                        ,FullRowSelect = true
                        ,GridLines     = true
                        ,LabelEdit     = true
                        )
        let mutable byUser = false // 本来ならイベントのsenderで判定処理すべき部分ぽい？
        let fromUser (f: unit -> unit) : unit = if byUser then f() else ()
        let byCode (f: unit -> unit) : unit =
            byUser <- false
            f()
            byUser <- true // マルチスレッドだとちょと怪しいかも
        do
            self.Columns.Add("Block/Comment").Width <- 180
            self.Columns.Add("*").Width <- -1
            self.Columns.Add("Host Name").Width <- -1
            self.ItemChecked |> Event.add (fun e ->
                fromUser <| fun () ->
                    let ig = e.Item.Tag :?>Ignored.Ignored
                    ig.Blocked <- e.Item.Checked
                    e.Item.SubItems.[1].Text <- modStars ig.Modified
                    notify !notifier ()
            )
            self.AfterLabelEdit |> Event.add (fun e ->
                fromUser <| fun () ->
                    match e.Label with
                    | null  -> ()
                    | label ->
                        let item = self.Items.[e.Item]
                        let ig = item.Tag :?> Ignored.Ignored
                        ig.Comment <- label
                        item.SubItems.[1].Text <- modStars ig.Modified
                        notify !notifier ()
            )
            let mutable lastCol = -1
            self.ColumnClick |> Event.add (fun e ->
                byCode <| fun () ->
                    let col = e.Column
                    let order = if col = lastCol then 1 else -1
                    lastCol <- if col = lastCol then -1 else col
                    self.ListViewItemSorter <-
                        { new System.Collections.Generic.Comparer<ListViewItem>() with
                                override self.Compare(a: ListViewItem, b: ListViewItem) : int =
                                    order * a.SubItems.[col].Text.CompareTo(b.SubItems.[col].Text)
                        }
                    self.ListViewItemSorter <- null
            )
        let add (item: Ignored.Ignored) : unit =
            new ListViewItem([|item.Comment; modStars item.Modified; item.Host|]
                            ,Checked = item.Blocked
                            ,Tag     = item
                            )
            |> self.Items.Add
            |> ignore
        member self.ShowItems (items: Ignored.Ignored seq) : unit =
            byCode <| fun () ->
                self.Items.Clear()
                Seq.iter add items


    type AddHosts(components: IContainer, notifier: Notifier<Ignored.Ignored array> ref) as self =
        inherit GroupBox(Text   = "Add:"
                        ,Height = 150
                        )
        let padding = 20
        let label1 = new Label(Left      = padding
                              ,Top       = padding
                              ,Width     = 70
                              ,Text      = "Comment:"
                              ,TextAlign = ContentAlignment.MiddleLeft
                              )
        let button1 = new Button(Left   = self.ClientRectangle.Width - 100 - padding
                                ,Top    = padding
                                ,Width  = 100
                                ,Anchor = (AnchorStyles.Top ||| AnchorStyles.Right)
                                ,Text   = "Add"
                                )
        let textBox1 = new TextBox(Left   = label1.Left + label1.Width
                                  ,Top    = padding
                                  ,Width  = self.ClientRectangle.Width - label1.Width - button1.Width - 2 * padding - 5
                                  ,Anchor = (AnchorStyles.Left ||| AnchorStyles.Top ||| AnchorStyles.Right)
                                  )
        let top = padding + Array.max [|label1.Height; button1.Height; textBox1.Height|]
        let label2 = new Label(Left      = padding
                              ,Top       = top
                              ,Width     = 70
                              ,Text      = "Hosts:"
                              ,TextAlign = ContentAlignment.MiddleLeft
                              )
        let textBox2 = new TextBox(Multiline = true
                                  ,Left      = label2.Left + label2.Width
                                  ,Top       = top
                                  ,Width     = self.ClientRectangle.Width - label2.Width - 2 * padding
                                  ,Height    = self.ClientRectangle.Height - top - padding / 2
                                  ,Anchor    = (AnchorStyles.Left ||| AnchorStyles.Top ||| AnchorStyles.Right ||| AnchorStyles.Bottom)
                                  )
        let pickHostname (line: string) : string option =
            let line = line.Trim()
            try
                let url = if line.StartsWith("http") then line else ("http://" + line)
                let uri = new System.Uri(url)
                Some uri.Host
            with
                _ -> None
        do
            setChildren components self [|label1; textBox1; button1; label2; textBox2|]
            let separator = [|'\n'; '\r'|]
            button1.Click |> Event.add (fun _ ->
                let comment = textBox1.Text.Trim()
                let generate hostname = new Ignored.Ignored(true, hostname, comment, true)
                textBox2.Text.Split(separator, System.StringSplitOptions.RemoveEmptyEntries)
                |> Array.toSeq
                |> Seq.map pickHostname
                |> Seq.filter Option.isSome
                |> Seq.map (Option.get >> generate)
                |> Seq.toArray
                |> notify !notifier
            )
        member val ToolTipTarget : Control = textBox2 :> Control


    let showMessage<'a> (res: Result<string, 'a>) : unit =
        let (msg, title) =
            match res with
            | Ok  msg -> (msg, appTitle())
            | Err err -> (sprintf "%A" err, "ERROR! - " + appTitle())
        MessageBox.Show(msg, title)
        |> ignore


    let save (hosts: Hosts.Hosts) (output: string) : Result<string, exn> =
        try
            let text = hosts.ToString()
            File.WriteAllText(output, text)
            Ok "Saved!"
        with
            err -> Err err


    let copy (hosts: Hosts.Hosts) : Result<string, exn> =
        try
            let text = hosts.ToString()
            Clipboard.SetText(text, TextDataFormat.Text)
            Ok "Copied!"
        with
            err -> Err err


    type MainForm(hosts: Hosts.Hosts, param: Param.Param) as self =
        inherit Form(StartPosition = FormStartPosition.CenterScreen
                    ,Width         = 500
                    ,Height        = 640
                    )
        let components = new Container()
        do
            let mainMenu = new MainMenu()
            mainMenu.MenuItems.Add("Save to Output").Click |> Event.add (fun _ ->
                save hosts param.Output |> showMessage
            )
            mainMenu.MenuItems.Add("Copy to Clipboard").Click |> Event.add (fun _ ->
                copy hosts |> showMessage
            )
            self.Menu <- mainMenu
        do
            let panel1 = new LTPanel(components
                                    ,Left    = 0
                                    ,Top     = 0
                                    ,Width   = self.ClientRectangle.Width
                                    ,Anchor  = (AnchorStyles.Left ||| AnchorStyles.Top ||| AnchorStyles.Right)
                                    ,Label   = "Input:"
                                    ,TextBox = param.Input
                                    )
            let top = panel1.Height
            let panel2 = new LTPanel(components
                                    ,Left    = 0
                                    ,Top     = top
                                    ,Width   = self.ClientRectangle.Width
                                    ,Anchor  = (AnchorStyles.Left ||| AnchorStyles.Top ||| AnchorStyles.Right)
                                    ,Label   = "Output:"
                                    ,TextBox = param.Output
                                    )
            let top = panel2.Top + panel2.Height
            let panel3 = new LTPanel(components
                                    ,Left    = 0
                                    ,Top     = top
                                    ,Width   = self.ClientRectangle.Width
                                    ,Anchor  = (AnchorStyles.Left ||| AnchorStyles.Top ||| AnchorStyles.Right)
                                    ,Label   = "Backup:"
                                    ,TextBox = param.Backup
                                    )
            let top = panel3.Top + panel3.Height
            let filter = new Filter(components
                                   ,Left   = 0
                                   ,Top    = top
                                   ,Width  = self.ClientRectangle.Width
                                   ,Anchor = (AnchorStyles.Left ||| AnchorStyles.Top ||| AnchorStyles.Right)
                                   )
            let appender : Notifier<Ignored.Ignored array> ref = ref None
            let panel4 = new AddHosts(components, appender)
            let notifier : Notifier<unit> ref = ref None
            let top = filter.Top + filter.Height
            let listView = new IgnoresView(notifier
                                          ,Left   = 0
                                          ,Top    = top
                                          ,Width  = self.ClientRectangle.Width
                                          ,Height = self.ClientRectangle.Height - top - panel4.Height
                                          ,Anchor = (AnchorStyles.Left ||| AnchorStyles.Top ||| AnchorStyles.Right ||| AnchorStyles.Bottom)
                                          )
            let top = listView.Top + listView.Height
            do
                panel4.Left   <- 0
                panel4.Top    <- top
                panel4.Width  <- self.ClientRectangle.Width
                panel4.Anchor <- (AnchorStyles.Left ||| AnchorStyles.Right ||| AnchorStyles.Bottom)
            let toolTip = new ToolTip(components)
            do // こういう設定のやり方はあまりよくない
                toolTip.SetToolTip(panel1.ToolTipTarget, "コマンドライン引数 -i <filepath> で指定可能")
                toolTip.SetToolTip(panel2.ToolTipTarget, "コマンドライン引数 -o <filepath> で指定可能")
                toolTip.SetToolTip(panel3.ToolTipTarget, "コマンドライン引数 -b <filepath> で指定可能")
                toolTip.SetToolTip(filter.ToolTipTarget, "半角スペース区切りのキーワードいずれかに一致")
                toolTip.SetToolTip(panel4.ToolTipTarget, "hostnameかURLを改行区切りで列挙")
            let updateTitle() =
                self.Text <-
                    sprintf
                        "%s%s (%d / %d)"
                        (if hosts.Modified then "*" else "")
                        (appTitle())
                        listView.CheckedItems.Count
                        listView.Items.Count
            let resetIgnores() : unit =
                let filterFunc = filter.GetFilterFunc()
                let ignores = Map.toSeq hosts.Ignores |> Seq.map snd
                listView.ShowItems(Seq.filter filterFunc ignores)
                updateTitle()
            resetIgnores()
            filter.ApplyButton.Click |> Event.add (fun _ -> resetIgnores())
            notifier := Some updateTitle
            appender := Some (fun ignores ->
                hosts.Update(ignores) |> listView.ShowItems
                updateTitle()
            )
            setChildren components self [|panel1; panel2; panel3; filter; listView; panel4|]
        override self.Dispose (disposing: bool) : unit =
            if disposing then components.Dispose() else ()
            base.Dispose(disposing)


module Program =
    let run (args: string array) : Result<unit, exn> =
        try
            result {
                let! param = Param.parse args
                let  lines = File.ReadAllLines param.Input
                File.Copy(param.Input, param.Backup, true)
                let! hosts = Hosts.parse lines
                Application.EnableVisualStyles()
                Application.SetCompatibleTextRenderingDefault(false)
                let form  = new UI.MainForm(hosts, param)
                Application.Run(form)
                return! Ok ()
            }
        with
            err -> Err err


    [<System.STAThread>]
    [<EntryPoint>]
    let main (args: string array) : int =
        match run args with
        | Ok  _   -> 0
        | Err err ->
            UI.showMessage (Err err)
            1
